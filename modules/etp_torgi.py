import requests
import json
from time import sleep
from bs4 import BeautifulSoup

PARSER_NAME = 'etp-torgi'

# Количество элементов на странице
#   на сайте варианты: 10, 20, 40. но можно и другие)
Number_of_items_per_page = '10'
keywords = []
banned_keywords = []
fields = []
base_url = 'https://www.etp-torgi.ru'


def read_key():
    with open('./extra/keywords.txt') as f:
        for line in f:
            if line != '\n' and line[0] != '#':
                keywords.append(line.strip())
    with open('./extra/banned_keywords.txt') as f:
        for line in f:
            if line != '\n' and line[0] != '#':
                banned_keywords.append(line.strip())
    with open('./extra/fields.txt') as f:
        for line in f:
            if line != '\n' and line[0] != '#':
                fields.append(line.strip())


def get_protocol(lot_info):
    sleep(2)
    res = requests.get(lot_info['protocol_url'])
    soup = BeautifulSoup(res.text, 'html.parser')
    lot = soup.find('tbody').find_all('tr')[int(lot_info['lot number']) - 1]
    if lot_info['type'] == 'Аукцион. Аренда государственного и муниципального имущества':
        lot = lot.find('div', class_="panel panel-default mte-form-panel")
        lot = lot.find_all('li', class_="list-group-item")
        temp = lot[-5].find('div', class_="col-sm-8")
        lot_info[lot[-5].find('div', class_="col-sm-4 control-label").get_text().strip()] = temp.get_text().strip()
    else:
        lot = lot.find_all('div', class_="panel panel-default mte-form-panel")[1].find('div', class_="row")
        temp = lot.find_all('table', class_="table table-striped")[1].find('tbody').find_all('td')
        s = 'Признать следующего Участника победителем торгов по лоту №' + lot_info['lot number'] +\
            '. Приступить к заключению с ним договора купли-продажи. Участник: ' + temp[1].get_text().strip() +\
            '. Предложение: ' + temp[2].get_text().strip()
        lot_info[lot.find('div', class_="col-sm-4").get_text().strip()[0:-1]] = s


def get_general_information(lot_info):
    sleep(2)
    res = requests.get(lot_info['href'])
    soup = BeautifulSoup(res.text, 'html.parser')

    if lot_info['type'] == 'Продажа имущества без объявления цены':
        lots = soup.find('main')
        lots = soup.find_all('div', class_="panel panel-default mte-form-panel lot_info")
    else:
        lots = soup.find_all('div', class_="lot_block")

    tmp = lots[int(lot_info['lot number']) - 1].find_all('div', class_="form-group")
    for z in tmp:
        left = z.find('label', class_="col-sm-4 control-label").get_text().strip()
        success = -1
        for line in fields:
            success = left.find(line)
            if success != -1:
                break
        if success == -1:
            continue
        lot_info[left] = \
            z.find('div', class_="form-control-static formInfo").get_text().strip()
    protocol = soup.find('ul', class_="nav nav-tabs")
    protocol_url = protocol.find_all('li')[-1]
    protocol_url = protocol_url.a.get('href')
    lot_info['protocol_url'] = base_url + protocol_url
    if lot_info['status'] == 'Торги состоялись':
        get_protocol(lot_info)


def info():
    return PARSER_NAME


def parse(queue):
    read_key()
    from_ = '0'
    url = "https://www.etp-torgi.ru/market/?action=search&search_type=all&search_record_on_page=" +\
          Number_of_items_per_page + "&currency=0&checkbox_privatization_auction=on" +\
          "&checkbox_privatization_public_offer2=on&checkbox_privatization_property_disposal=on" +\
          "&checkbox_privatization_realization=on&checkbox_privatization_confiscated=on" +\
          "&checkbox_rent_auction=on&checkbox_two_parts_auction=on&from="
    res = requests.get(url + from_ + "&page=1")
    soup = BeautifulSoup(res.text, 'html.parser')
    last_page = soup.find('ul', class_="pagination pull-left").find_all('li')[-1].a.get('href')
    last_page = int(last_page[(last_page.rfind('page=') + 5):])
    for page in range(1, last_page + 1):
        if page != 1:
            from_ = int(Number_of_items_per_page) * page - 1
            res = requests.get(url + str(from_) + "&page=" + str(page))
            sleep(2)
            soup = BeautifulSoup(res.text, 'html.parser')
        tr_elements = soup.find_all('tr', class_="c1")
        for i in range(0, int(Number_of_items_per_page)):
            lot = tr_elements[i].find_all('td')
            # 0 - Тип аукциона
            # 1 - Номер аукциона
            # 2 - Объект торгов
            # 3 - Организатор аукциона
            # 4 - Месторасположение
            # 5 - Начальная цена
            # 6 - Дата публикации, Дата окончания приема заявок, Дата рассмотрения заявок,
            #       Дата начала аукциона, Дата подведения итогов торгов
            # 7 - Состояние
            success_k = 0
            for x in keywords:
                success_k = lot[2].get_text().find(x)
                if success_k != -1:
                    break
            if success_k == -1:
                continue
            success_bk = 0
            for x in banned_keywords:
                success_bk = lot[2].get_text().find(x)
                if success_bk != -1:
                    break
            if success_bk != -1:
                continue
            lot_info = {}
            lot_info['type'] = lot[0].get_text()
            lot_info['auction number'] = lot[1].get_text().split('-')[0][1:]
            lot_info['lot number'] = lot[1].get_text().split('-')[1][0:-1]
            lot_info['href'] = base_url + lot[1].a.get('href')
            lot_info['organizer'] = lot[3].a.get_text()
            lot_info['organizer_href'] = base_url + lot[3].a.get('href')
            a = lot[6].find_all('span')
            for x in a:
                lot_info[x.get('title').strip()] = x.get_text().strip()
            lot_info['status'] = lot[7].get_text()
            get_general_information(lot_info)

            queue.put_nowait((PARSER_NAME, lot_info))
    queue.put_nowait(('stop:' + PARSER_NAME, None))