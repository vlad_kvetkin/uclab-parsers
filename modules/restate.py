from selenium import webdriver, common
from bs4 import BeautifulSoup
import requests
from random import choice, uniform
from time import sleep

PARSER_NAME = 'restate'

def info():
    return PARSER_NAME

def get_info(link, type):
    list = {}
    list['ADVERTISEMENT_TYPE'] = type

    html = requests.get(link)
    soup = BeautifulSoup(html.text, 'html.parser')

    header = soup.find('div', class_='main-block baseobj-block clearfix')
    if header:
        head = header.find('h1', class_='red-header').text
        list['TITLE'] = head

    top_info = soup.find('div', class_='top-info')
    if top_info:
        date = top_info.find('span', class_='date').text
        list['DATE_CREATE'] = date[2:10]

    main_params = soup.find('div', class_='main-params')
    if main_params:
        room = main_params.find('span').text
        list['ROOM_COUNT'] = room[:1]

    pricec = soup.find('div', class_='price')
    if pricec:
        price = pricec.find('span').text
        list['PRICE'] = price[:-2]

    location = soup.find('div', attrs={'class': 'location'}).text
    addres = location.split(', ')
    address = ''
    i = 0
    while (i < 3):
        address = address + addres[i].replace('\n', '').replace('на карте', '') + ', '
        i = i + 1
    list['ADDRESS'] = address

    seller = str(soup.find('span', {'style': "font-weight:bold; color:#666;"}).get_text())

    list['SELLER_INFO'] = seller

    value = soup.findAll('td')
    i = 0
    for e in value:
        if (e.text == "Этаж"):
            list['FLOOR'] = value[i + 1].text[:2]
            list['TOTAL_FLOOR'] = value[i + 1].text
        elif (e.text == "Площадь"):
            list['TOTAL_AREA'] = value[i + 1].text[0:-2]
        elif (e.text == "Жилая"):
            list['LIVING_AREA'] = value[i + 1].text[0:-2]
        elif (e.text == "Комнат"):
            list['ROOM_AREA'] = value[i + 1].text[0:-2]
        elif (e.text == "Кухня"):
            list['KITCHEN_AREA'] = value[i + 1].text[0:-2]
        elif (e.text == "Состояние"):
            list['STATE'] = value[i + 1].text
        elif (e.text == "Санузел"):
            list['BATHROOM_TYPE'] = value[i + 1].text
        elif (e.text == "Лифт"):
            list['ELEVATOR'] = value[i + 1].text
        elif (e.text == "Тип дома"):
            list['HOME_TYPE'] = value[i + 1].text
        elif (e.text == "Балкон"):
            list['BALCONY'] = value[i + 1].text
        elif (e.text == "Паркинг"):
            list['PARKING'] = value[i + 1].text
        i = i + 1

    try:
        options = webdriver.FirefoxOptions()
        options.add_argument('-headless')
        driver = webdriver.Firefox(firefox_options=options, executable_path="geckodriver/geckodriver.exe")
        driver.get(link)
        url = driver.find_element_by_xpath('/html/body/div[8]/div/div[1]/div[3]/div[4]/div[2]/div/div[6]/span[2]')
        url.click()
        sleep(uniform(2, 4))

        phone = driver.find_element_by_id('number').text
        driver.close()
        list['SELLER_PHONE'] = phone
    except common.exceptions.NoSuchElementException:
        return list

    return list

def getHtml(page, queue):
    html = requests.get(page)
    soup1 = BeautifulSoup(html.text, 'html.parser')
    offerURLs = soup1.find_all("div", attrs={"class": "object-info"})
    for item in offerURLs:
        href = item.find('a')['href']
        type = "Продажа"
        data = get_info(href, type)
        queue.put_nowait((PARSER_NAME, data))

def parse(queue):
    for i in range(1, 2):  # парсинг покупок квартир
        page = f'https://volgograd.restate.ru/poisk/?object=sareas-1&'\
                'price_from=0&price_to=0&pricetype=1&s_from=0&s_to=0&'\
                'region=60345&street=&floor=&area-region=&locationinp=&'\
                'districtinp=&metroinp=&regiondopinp=&pointinp=&'\
                'railinp=&dealid=2&page={0}'.format(i)
        getHtml(page, queue)
    queue.put_nowait(('stop:' + PARSER_NAME, None))