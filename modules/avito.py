from bs4 import BeautifulSoup
import requests
from modules import proxy
from random import choice, uniform
from time import sleep

PARSER_NAME = 'avito'

HEADERS = {
    'User-Agent': 'Mozilla/5.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'Host': 'www.avito.ru',
    'Upgrade-Insecure-Requests': '1',
    'TE': 'Trailers'
}

UAS = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17',
    'Mozilla/5.0 (Linux; U; Android 2.2; fr-fr; Desire_A8181 Build/FRF91) App3leWebKit/53.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FunWebProducts; .NET CLR 1.1.4322; PeoplePal 6.2)',
    'Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1',
    'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.01',
    'Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; .NET CLR 3.5.30729)'
]

def info():
    return PARSER_NAME

def parse(queue):
    startUrl = 'https://www.avito.ru/rossiya/nedvizhimost'
    proxyDict = proxy.Proxy().get_proxy()
    headers = HEADERS
    headers['User-Agent'] = choice(UAS)

    r1 = requests.get(startUrl, proxies=proxyDict, headers=headers)
    soup1 = BeautifulSoup(r1.text, 'html.parser')

    # Если прокси забанили
    while soup1.select_one('.firewall-container'):
        proxyDict = proxy.Proxy().get_proxy()
        headers['User-Agent'] = choice(UAS)

        sleep(uniform(3, 6))
        r1 = requests.get(startUrl, proxies=proxyDict, headers=headers)

        soup1 = BeautifulSoup(r1.text, 'html.parser')

    # ищу все ссылки на объявления на данной странице
    for names in soup1.findAll('a', {'data-marker': 'title'}):
        advertisementData = {}
        # url одного конкретного объявления
        note_url = 'https://www.avito.ru' + names.attrs['href']
        # переход по ссылке на конкретное объявление
        sleep(uniform(3, 6))
        try:
            r2 = requests.get(note_url, proxies=proxyDict, headers=headers)
        except BaseException:
            continue

        soup2 = BeautifulSoup(r2.text, 'html.parser')

        for item in soup2.find_all('li', 'item-params-list-item'):
            for i in item.find_all('span', 'item-params-label'):
                if (len(item.text.split(':')) > 1):
                    itemData = item.text.split(':')[1]
                    itemData = itemData.strip()
                    itemKey = i.text.strip()
                    itemKey = itemKey.replace(':', '')
                    if (itemKey == 'Этаж'):
                        advertisementData['FLOOR'] = itemData
                    elif (itemKey == 'Этажей в доме'):
                        advertisementData['TOTAL_FLOOR'] = itemData
                    elif (itemKey == 'Тип дома'):
                        advertisementData['HOME_TYPE'] = itemData
                    elif (itemKey == 'Количество комнат'):
                        advertisementData['ROOM_COUNT'] = itemData
                    elif (itemKey == 'Общая площадь'):
                        advertisementData['TOTAL_AREA'] = itemData
                    elif (itemKey == 'Жилая площадь'):
                        advertisementData['LIVING_AREA'] = itemData
                    elif (itemKey == 'Площадь кухни'):
                        advertisementData['KITCHEN_AREA'] = itemData
                    elif (itemKey == 'Тип участия'):
                        advertisementData['PARTICIPATION_TYPE'] = itemData
                    elif (itemKey == 'Официальный застройщик'):
                        advertisementData['OFFICIAL_BUILDER'] = itemData
                    elif (itemKey == 'Название новостройки'):
                        advertisementData['NEW_BUILDING_NAME'] = itemData
                    elif (itemKey == 'Корпус, строение'):
                        advertisementData['STRUCTURE'] = itemData
                    elif (itemKey == 'Отделка'):
                        advertisementData['DECORATION'] = itemData


        descr = soup2.find_all('p')
        for t in descr:
            advertisementData['DESCRIPTION'] = t.text.strip()

        title = soup2.find_all('span', 'title-info-title-text')
        for t in title:
            advertisementData['TITLE'] = t.text.strip()

        price = soup2.find_all('span', 'price-value-string', 'js-price-value-string')
        for t in price:
            advertisementData['PRICE'] = t.text.strip()

        main1 = soup2.find('div', 'seller-info-name')
        main2 = soup2.find('div', 'seller-info-prop')
        if hasattr(main1, 'text'):
            if hasattr(main2, 'text'):
                advertisementData['INFORMATION'] = main2.text.strip()
            else:
                advertisementData['INFORMATION']: main1.text.strip()

        for item in soup2.find_all('span', 'seller-info-prop'):
            if (item.find('div', 'seller-info-label').text == 'Контактное лицо'):
                advertisementData['SELLER_INFO'] = item.find('div', 'seller-info-value').text.strip()

        address = soup2.find_all('div', 'item-map-location')
        for t in address:
            advertisementData['ADDRESS'] = t.text.strip()

        map0 = soup2.find('div', 'item-map', 'js-item-map')
        if hasattr(map0, 'find'):
            map = map0.find('div', 'b-search-map')
            advertisementData['LATITUDE'] = map.attrs['data-map-lat'].strip()
            advertisementData['LONGITUDE'] = map.attrs['data-map-lon'].strip()

        for item in soup2.find_all('li', 'advanced-params-param'):
            title1 = item.find('div', 'advanced-params-param-title').text.strip()
            n = 0
            for i in item.find_all('li', 'advanced-params-param-item'):
                n += 1
                advertisementData[title1.strip() + str(n)] = i.text.strip()

        queue.put_nowait((PARSER_NAME, advertisementData))

    queue.put_nowait(('stop:' + PARSER_NAME, None))
