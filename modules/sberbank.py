import requests
import json
import math
import re
import os

PARSER_NAME = 'sberbank'

input_file = './extra/searches.txt'
xml_data_req = """
<?xml version="1.0" encoding="UTF-8"?>
<elasticrequest>
   <filters>
      <mainSearchBar>
         <value>{0}</value>
         <type>best_fields</type>
         <minimum_should_match>100%</minimum_should_match>
      </mainSearchBar>
      <purchAmount>
         <minvalue />
         <maxvalue />
      </purchAmount>
      <PublicDate>
         <minvalue />
         <maxvalue />
      </PublicDate>
      <PurchaseStageTerm>
         <value />
         <visiblepart />
      </PurchaseStageTerm>
      <CustomerCondition>
         <value />
      </CustomerCondition>
      <CustomerDictionary>
         <value />
      </CustomerDictionary>
      <customer>
         <visiblepart />
      </customer>
      <RegionNameTerm>
         <value />
         <visiblepart />
      </RegionNameTerm>
      <RequestStartDate>
         <minvalue />
         <maxvalue />
      </RequestStartDate>
      <RequestDate>
         <minvalue />
         <maxvalue />
      </RequestDate>
      <AuctionBeginDate>
         <minvalue />
         <maxvalue />
      </AuctionBeginDate>
      <okdp2MultiMatch>
         <value />
      </okdp2MultiMatch>
      <okdp2tree>
         <value />
         <productField />
         <branchField />
      </okdp2tree>
      <classifier>
         <visiblepart />
      </classifier>
      <orgCondition>
         <value />
      </orgCondition>
      <orgDictionary>
         <value />
      </orgDictionary>
      <organizator>
         <visiblepart />
      </organizator>
      <purchStateNameTerm>
         <value />
         <visiblepart />
      </purchStateNameTerm>
      <BidStatusName>
         <value />
         <visiblepart />
      </BidStatusName>
      <PurchaseWayTerm>
         <value />
         <visiblepart />
      </PurchaseWayTerm>
      <PurchaseTypeNameTerm>
         <value />
         <visiblepart />
      </PurchaseTypeNameTerm>
      <BranchNameTerm>
         <value />
         <visiblepart />
      </BranchNameTerm>
      <IsSMPTerm>
         <value />
         <visiblepart />
      </IsSMPTerm>
      <statistic>
         <totalProc>3     222</totalProc>
         <TotalSum>47.93 Млрд.</TotalSum>
         <DistinctOrgs>197</DistinctOrgs>
      </statistic>
   </filters>
   <fields>
      <field>TradeSectionId</field>
      <field>purchAmount</field>
      <field>purchCurrency</field>
      <field>purchCodeTerm</field>
      <field>purchCode</field>
      <field>PurchaseTypeName</field>
      <field>purchStateName</field>
      <field>OrgName</field>
      <field>SourceTerm</field>
      <field>PublicDate</field>
      <field>RequestDate</field>
      <field>RequestStartDate</field>
      <field>RequestAcceptDate</field>
      <field>AuctionBeginDate</field>
      <field>CreateRequestHrefTerm</field>
      <field>CreateRequestAlowed</field>
      <field>purchName</field>
      <field>SourceHrefTerm</field>
      <field>objectHrefTerm</field>
      <field>needPayment</field>
      <field>IsSMP</field>
      <field>isIncrease</field>
      <field>IntegratorCode</field>
      <field>IntegratorCodeTerm</field>
      <field>PurchaseExplanationRequestHrefTerm</field>
      <field>PurchaseId</field>
   </fields>
   <sort>
      <value>default</value>
      <direction />
   </sort>
   <aggregations>
      <empty>
         <filterType>filter_aggregation</filterType>
         <field />
         <min_doc_count>0</min_doc_count>
         <order>asc</order>
      </empty>
   </aggregations>
   <size>20</size>
   <from>{1}</from>
</elasticrequest>
"""


def info():
    return PARSER_NAME


def parse(queue):
    if not os.path.exists(input_file):
        raise Exception('Файла с поисковыми запросами не существует!')

    searches = open(input_file).read().split('\n')
    ads = []

    for s in searches:
        processed_count = 0
        amount_s_pages = 1
        while processed_count < amount_s_pages:
            data = {'xmlData': xml_data_req.format(s, processed_count * 20), 'orgId': '0'}

            resp = requests.post(
                'http://utp.sberbank-ast.ru/VIP/SearchQuery/PurchaseList',
                data,
                headers={'X-Requested-With': 'XMLHttpRequest'},
                timeout=30)

            amount_s_pages = int(math.ceil(int(resp.json()['data']['Data']['pagerTotal']) / 20))

            ids = re.findall(r'objectHrefTerm\\u003e(.*?)\\u003c\/objectHrefTerm', resp.text)

            for i in ids:
                ads.append(i)

            processed_count += 1

    for ad in ads:
        print('Start process ad: ' + ad)

        resp = requests.get(
            ad,
            headers={
                "User-Agent":
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36"
            },
            timeout=30).text

        data = {}

        data["Ссылка на объявление"] = ad

        try:
            data['Начальная цена процедуры'] = re.findall(r'PurchaseAmount&gt;(.*?)&lt;\/PurchaseAmount', resp)[0]
        except IndexError:
            pass

        try:
            data['Проводить торги при подачи двух и более подтверждений цены периода'] = re.findall(
                r'PurchaseInfo_PurchaseAmount\' class=\'\' data-defaultValue=\'(?:.*?)\' style=\'\'>(.*?)<', resp)[0]
        except IndexError:
            pass

        try:
            data['Проводить периоды при единственной допущенной заявке'] = re.findall(
                r'PurchasePropertyNeedPeriod&gt;(.*?)&lt;\/PurchasePropertyNeedPeriod', resp)[0]
        except IndexError:
            pass

        try:
            data['Окончание рассмотрения заявок на участие'] = re.findall(
                r'RequestReviewDate&gt;(.*?)&lt;\/RequestReviewDate', resp)[0]
        except IndexError:
            pass

        try:
            data['Полное наименование заказчика'] = re.findall(r'customerfullname&gt;(.*?)&lt;\/customerfullname',
                                                               resp)[0]
        except IndexError:
            pass

        try:
            data['Фактический адрес (почтовый)'] = re.findall(r'customeraddressfact&gt;(.*?)&lt;\/customeraddressfact',
                                                              resp)[0],
        except IndexError:
            pass

        try:
            data['Тип процедуры'] = re.findall(r'PurchaseTypeName&gt;(.*?)&lt;\/PurchaseTypeName', resp)[0]
        except IndexError:
            pass

        try:
            data['Номер процедуры'] = re.findall(r'PurchaseCode&gt;(.*?)&lt;\/PurchaseCode', resp)[0]
        except IndexError:
            pass

        try:
            data['Вариант проведения процедуры'] = re.findall(
                r'PurchaseAccessLevelName&gt;(.*?)&lt;\/PurchaseAccessLevelName', resp)[0]
        except IndexError:
            pass

        try:
            data['Наименование процедуры'] = re.findall(r'PurchaseName&gt;(.*?)&lt;\/PurchaseName', resp)[0]
        except IndexError:
            pass

        try:
            data['Регион'] = re.findall(r'RegionIdName&gt;(.*?)&lt;\/RegionIdName', resp)[0]
        except IndexError:
            pass

        try:
            data['Адрес электронной площадки в сети "Интернет"'] = re.findall(
                r'(?:PurchaseInfoView_Site|PurchaseInfo_Site)\' class=\'\' data-defaultValue=\'(?:.*?)\' style=\'\'>(.*?)<',
                resp)[0]
        except IndexError:
            pass

        try:
            data['Статус процедуры'] = re.findall(r'PurchaseStatus&gt;(.*?)&lt;\/PurchaseStatus', resp)[0]
        except IndexError:
            pass

        try:
            data['Наименование организатора'] = re.findall(r'orgname&gt;(.*?)&lt;\/orgname', resp)[0]
        except IndexError:
            pass

        try:
            data['Полное наименование организатора'] = re.findall(r'orgfullname&gt;(.*?)&lt;\/orgfullname', resp)[0]
        except IndexError:
            pass

        try:
            data['ИНН организатора'] = re.findall(r'orginn&gt;(.*?)&lt;\/orginn', resp)[0]
        except IndexError:
            pass

        try:
            data['КПП организатора'] = re.findall(r'orgkpp&gt;(.*?)&lt;\/orgkpp', resp)[0]
        except IndexError:
            pass

        try:
            data['ОГРН организатора'] = re.findall(r'orgogrn&gt;(.*?)&lt;\/orgogrn', resp)[0]
        except IndexError:
            pass

        try:
            data['Юридический адрес (место нахождения)'] = re.findall(r'orgaddressjur&gt;(.*?)&lt;\/orgaddressjur',
                                                                      resp)[0]
        except IndexError:
            pass

        try:
            data['Фактический адрес (почтовый)'] = re.findall(r'orgaddressfact&gt;(.*?)&lt;\/orgaddressfact', resp)[0]
        except IndexError:
            pass

        try:
            data['Адрес электронной почты'] = re.findall(r'orgemail&gt;(.*?)&lt;\/orgemail', resp)[0]
        except IndexError:
            pass

        try:
            data['Номер контактного телефона'] = re.findall(r'orgphone&gt;(.*?)&lt;\/orgphone', resp)[0]
        except IndexError:
            pass

        try:
            data['Контактное лицо'] = re.findall(r'orgcontactperson&gt;(.*?)&lt;\/orgcontactperson', resp)[0]
        except IndexError:
            pass

        try:
            data['Наименование заказчика'] = re.findall(r'customernickname&gt;(.*?)&lt;\/customernickname', resp)[0]
        except IndexError:
            pass

        try:
            data['ИНН заказчика'] = re.findall(r'customerinn&gt;(.*?)&lt;\/customerinn', resp)[0]
        except IndexError:
            pass

        try:
            data['КПП заказчика'] = re.findall(r'customerkpp&gt;(.*?)&lt;\/customerkpp', resp)[0]
        except IndexError:
            pass

        try:
            data['Юридический адрес (место нахождения)'] = re.findall(
                r'customeraddressjur&gt;(.*?)&lt;\/customeraddressjur', resp)[0]
        except IndexError:
            pass

        try:
            data['Начало подачи заявок на участие'] = re.findall(r'RequestStartDate&gt;(.*?)&lt;\/RequestStartDate',
                                                                 resp)[0]
        except IndexError:
            pass

        try:
            data['Окончание подачи заявок на участие'] = re.findall(r'RequestStopDate&gt;(.*?)&lt;\/RequestStopDate',
                                                                    resp)[0]
        except IndexError:
            pass

        queue.put_nowait((PARSER_NAME, data))
    queue.put_nowait(('stop:' + PARSER_NAME, None))
