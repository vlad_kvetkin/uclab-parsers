from bs4 import BeautifulSoup
from selenium import webdriver
import urllib.request
import json

baseUrl = 'http://domino-rf.ru'

PARSER_NAME = 'domino-rf'


def fillingFields_Sale_Flat(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этаж'):
                Declaration['floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этажность'):
                Declaration['total_floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Жилая площадь'):
                try:
                    Declaration['living_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['living_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь кухни'):
                try:
                    Declaration['kitchen_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['kitchen_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Тип дома'):
                Declaration['home_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Ремонт'):
                Declaration['repairs'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие городского телефона'):
                Declaration['phone'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Балкон/лоджия'):
                Declaration['balcony'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Состояние дома'):
                Declaration['condition'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие лифта'):
                Declaration['elevator'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Sale_Room(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Площадь комнаты'):
                try:
                    Declaration['room_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['area room'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этаж'):
                Declaration['floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этажность'):
                Declaration['total_floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Жилая площадь'):
                try:
                    Declaration['living_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['living_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь кухни'):
                try:
                    Declaration['kitchen_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['kitchen_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Тип дома'):
                Declaration['home_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Ремонт'):
                Declaration['repairs'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Балкон/лоджия'):
                Declaration['balcony'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Состояние дома'):
                Declaration['condition'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие лифта'):
                Declaration['elevator'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Sale_House(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь участка'):
                try:
                    Declaration['land_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['land_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Продаваемая часть недвижимости'):
                Declaration['share_size'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Удобства'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Материал стен'):
                Declaration['wall_material'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие канализации'):
                Declaration['sewerage'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Проведена вода'):
                Declaration['water_supply'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Проведен газ'):
                Declaration['gas'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие отопления'):
                Declaration['heating'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Sale_Commercial(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))


def fillingFields_Sale_nonResidential_Garage(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Длина гаража'):
                try:
                    Declaration['length'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['length'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Ширина гаража'):
                try:
                    Declaration['width'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['width'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Материал стен'):
                Declaration['wall_material'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Sale_nonResidential_Dacha(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Садовое товарищество'):
                Declaration['garden_community'] = col[1].text.strip('\n\t ')
            elif (characteristic == 'Площадь участка'):
                try:
                    Declaration['land_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['land_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Проведена вода'):
                Declaration['water_supply'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Проведен газ'):
                Declaration['gas'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Sale_nonResidential_Other(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        pass


def fillingFields_Sale_Building_land(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Площадь участка'):
                try:
                    Declaration['land_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['land_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ')
            elif (characteristic == 'Проведена вода'):
                Declaration['water_supply'] = col[1].text.strip('\n\t ')
            elif (characteristic == 'Проведен газ'):
                Declaration['gas'] = col[1].text.strip('\n\t ')


def fillingFields_Rental_Flat(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этаж'):
                Declaration['floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этажность'):
                Declaration['total_floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Жилая площадь'):
                try:
                    Declaration['living_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['living_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь кухни'):
                try:
                    Declaration['kitchen_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['kitchen_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Тип дома'):
                Declaration['home_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Ремонт'):
                Declaration['repairs'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие городского телефона'):
                Declaration['phone'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Балкон/лоджия'):
                Declaration['balcony'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Состояние дома'):
                Declaration['condition'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие лифта'):
                Declaration['elevator'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Rental_Room(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Площадь комнаты'):
                try:
                    Declaration['room_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['room_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этаж'):
                Declaration['floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этажность'):
                Declaration['total_floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Жилая площадь'):
                try:
                    Declaration['living_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['living_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь кухни'):
                try:
                    Declaration['kitchen_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['kitchen_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Тип дома'):
                Declaration['home_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Ремонт'):
                Declaration['repairs'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Балкон/лоджия'):
                Declaration['balcony'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Состояние дома'):
                Declaration['condition'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие лифта'):
                Declaration['elevator'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Rental_House(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Количество комнат'):
                Declaration['room_count'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь участка'):
                try:
                    Declaration['land_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['land_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Продаваемая часть недвижимости'):
                Declaration['share_size'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Материал стен'):
                Declaration['wall_material'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие канализации'):
                Declaration['sewerage'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Проведена вода'):
                Declaration['water_supply'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Газ'):
                Declaration['gas'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие отопления'):
                Declaration['heating'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Rental_Commercial(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))


def fillingFields_Rental_nonResidential_Garage(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'ГСК'):
                Declaration['gsk'] = col[1].text.strip('\n\t ')
            elif (characteristic == 'Длина гаража'):
                try:
                    Declaration['length'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['length'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Ширина гаража'):
                try:
                    Declaration['width'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['width'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Материал стен'):
                Declaration['wall_material'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие электричества'):
                Declaration['electricity'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Rental_nonResidential_Dacha(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        pass


def fillingFields_Purchase_Residential_Properties(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Продаваемая часть недвижимости'):
                Declaration['share_size'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Purchase_Commercial_Property(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Тип коммерческого объекта'):
                Declaration['object_type'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Exchange_Residential_Properties(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Общая площадь'):
                try:
                    Declaration['total_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['total_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этаж'):
                Declaration['floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Этажность'):
                Declaration['total_floor'] = int(col[1].text.strip('\n\t '))
            elif (characteristic == 'Жилая площадь'):
                try:
                    Declaration['living_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['living_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Площадь кухни'):
                try:
                    Declaration['kitchen_area'] = int(col[1].text.strip('\n\t '))
                except ValueError:
                    Declaration['kitchen_area'] = float(col[1].text.strip('\n\t '))
            elif (characteristic == 'Тип дома'):
                Declaration['home_type'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Ремонт'):
                Declaration['repairs'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Санузел'):
                Declaration['bathroom'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие городского телефона'):
                Declaration['phone'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Балкон/лоджия'):
                Declaration['balcony'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Наличие лифта'):
                Declaration['elevator'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Состояние дома'):
                Declaration['condition'] = col[1].text.strip('\n\t ').capitalize()
            elif (characteristic == 'Продаваемая часть недвижимости'):
                Declaration['share_size'] = col[1].text.strip('\n\t ').capitalize()


def fillingFields_Rent_Residential_Properties(soup, Declaration):
    row = soup.find_all('tr')
    if (len(row) != 0):
        for i in row:
            col = i.find_all('td')
            characteristic = col[0].text.strip('\n\t ')

            if (characteristic == 'Продаваемая часть недвижимости'):
                Declaration['share_size'] = col[1].text.strip('\n\t ')


def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()


def getPageCount(html):
    soup = BeautifulSoup(html, 'html.parser')

    div = soup.find('div', class_='pull-left').text.strip('\n\t ')
    countDeclaration = ''.join([i if i.isdigit() else ' ' for i in div])
    countDeclaration = countDeclaration.split()
    countDeclaration = int(countDeclaration[-1])

    div = soup.find('div', class_='pull-right')
    return int(div.find_all('li')[-2].text)
    # return countDeclaration//getCountDeclarationOnPage(html) + 1


def getCountFloors(subHeading, Declaration):
    SubHeading = subHeading.text.strip('\n\t ')

    # Обработка случаев: "*число*-эт"
    if (SubHeading.find('-эт') != -1):
        pos = SubHeading.find('-эт')
        floor = ''
        count = 1
        while True:
            if (SubHeading[pos - count].isdigit()):
                floor = floor + SubHeading[pos - count]
                count += 1
            else:
                break
        if (len(floor) == 1):
            Declaration['total_floor'] = int(floor)
        else:
            Declaration['total_floor'] = int(floor[::-1])
    # Обработка случаев: "*число*-ур"
    elif ((SubHeading.find('-ур') != -1)):
        pos = SubHeading.find('-ур')
        floor = ''
        count = 1
        while True:
            # Пока идут числа копируем в строку floor
            if (SubHeading[pos - count].isdigit()):
                floor = floor + SubHeading[pos - count]
                count += 1
            # Есть случаи дробного чиса уровней, так что обрабатываем случай с ","
            if (SubHeading[pos - count] == ','):
                floor = floor + ','
                count += 1
                while True:
                    if (SubHeading[pos - count].isdigit()):
                        floor = floor + SubHeading[pos - count]
                        count += 1
                    else:
                        break
                break
            # Иначе выходим
            else:
                break
        if (len(floor) == 1):
            Declaration['total_floor'] = int(floor)
        else:
            try:
                Declaration['total_floor'] = int(floor[::-1])
            except ValueError:
                Declaration['total_floor'] = float(floor[::-1])


def getCountDeclarationOnPage(html):
    soup = BeautifulSoup(html, 'html.parser')
    divContent = soup.find('div', class_='col-md-9 cusDirStyle')
    massItm = divContent.findAll('div', class_='col-md-12 catItem unpad')
    massItm = massItm + divContent.findAll('div', class_='col-md-12 catItem unpad premium')
    return len(massItm)


def parsing(html, urlDeclaration, subHeading=None):

    Declaration = {}

    soup = BeautifulSoup(html, 'html.parser')

    #------------------Цена и Цена за метр (если есть)----------------------------------------------
    price = soup.find('p', class_='price')
    if (price is not None):
        price = int(price.text[:-5].replace(' ', ''))
        Declaration['price'] = int(soup.find('p', class_='price').text[:-5].replace(' ', ''))

    meterPrice = soup.find('p', class_='meterPrice')
    if (meterPrice is not None):
        meterPrice = meterPrice.text[:-13]
        try:
            Declaration['price_per_m2'] = int(meterPrice)
        except ValueError:
            pass

    #------------------Теги поиска------------------------------------------------------------------
    keyWords = soup.find('div', class_='col-md-6 unpad')
    keyWords = keyWords.find('a').text.split('/')
    for i in range(len(keyWords)):
        keyWords[i] = keyWords[i].strip('\n\t ')

    #------------------Описание от продавца---------------------------------------------------------
    description = soup.find('blockquote')
    if (description is not None):
        Declaration['description'] = description.text.strip('\n\t ')
    else:
        Declaration['description'] = ''

    #------------------Дата опубликования-----------------------------------------------------------
    Declaration['date_of_public'] = soup.find('p', class_='detailDate').text[14:24]

    #------------------Адрес объекта----------------------------------------------------------------
    address = soup.find('div', class_='col-md-7').find('p')
    if (address is not None):
        Declaration['address'] = address.text

    #------------------Телефон продавца-------------------------------------------------------------

    panel_body_phone = soup.find('div', class_='panel-body')
    if (panel_body_phone is not None):
        try:
            options = webdriver.FirefoxOptions()
            options.add_argument('-headless')
            driver = webdriver.Firefox(firefox_options=options)
            driver.get(urlDeclaration)
            element = driver.find_element_by_id("showPhoneNumber")
            element.click()
            newElement = driver.find_element_by_id("phoneNumberBlock")
            phone = newElement.text.split(',')[0]
        except:
            phone = ''
        Declaration['seller_phone'] = phone

    #------------------URL объявления---------------------------------------------------------------
    if urlDeclaration is not None:
        Declaration['url'] = urlDeclaration

    #------------------Основные поля----------------------------------------------------------------
    if (len(keyWords) >= 3):
        advert_type = ''
        if (keyWords[1] == 'продажа'):
            advert_type += 'Купить '
            if (keyWords[2] == 'квартиры'):
                advert_type += 'квартиру'
                Declaration['advert_type'] = advert_type
                fillingFields_Sale_Flat(soup, Declaration)
            elif (keyWords[2] == 'комнаты'):
                advert_type += 'комнату'
                Declaration['advert_type'] = advert_type
                fillingFields_Sale_Room(soup, Declaration)
            elif (keyWords[2] == 'дома'):
                advert_type += 'дом'
                Declaration['advert_type'] = advert_type
                if (subHeading is not None):
                    getCountFloors(subHeading, Declaration)
                fillingFields_Sale_House(soup, Declaration)
            elif (keyWords[2] == 'коммерческая недвижимость'):
                advert_type += 'офис'
                Declaration['advert_type'] = advert_type
                fillingFields_Sale_Commercial(soup, Declaration)
            elif (keyWords[2] == 'нежилая недвижимость'):
                if (keyWords[3] == 'гаражи, стоянки'):
                    advert_type += 'гараж/стоянку'
                    Declaration['advert_type'] = advert_type
                    fillingFields_Sale_nonResidential_Garage(soup, Declaration)
                elif (keyWords[3] == 'дачи, участки'):
                    advert_type += 'дачу, участок'
                    Declaration['advert_type'] = advert_type
                    if (subHeading is not None):
                        getCountFloors(subHeading, Declaration)
                    fillingFields_Sale_nonResidential_Dacha(soup, Declaration)
                elif (keyWords[3] == 'другие объекты'):
                    advert_type += 'другой объект'
                    Declaration['advert_type'] = advert_type
                    fillingFields_Sale_nonResidential_Other(soup, Declaration)
            elif (keyWords[2] == 'участки под строительство'):
                advert_type += 'участок под строительство'
                Declaration['advert_type'] = advert_type
                fillingFields_Sale_Building_land(soup, Declaration)

        elif (keyWords[1] == 'сдаю'):
            advert_type += 'Снять '
            if (keyWords[2] == 'квартиры посуточно'):
                advert_type += 'посуточно квартиру'
                Declaration['advert_type'] = advert_type
                fillingFields_Rental_Flat(soup, Declaration)
            elif (keyWords[2] == 'квартиры на длительный срок'):
                advert_type += 'квартиру'
                Declaration['advert_type'] = advert_type
                fillingFields_Rental_Flat(soup, Declaration)
            elif (keyWords[2] == 'комнаты'):
                advert_type += 'комнату'
                Declaration['advert_type'] = advert_type
                fillingFields_Rental_Room(soup, Declaration)
            elif (keyWords[2] == 'дома'):
                advert_type += 'дом'
                Declaration['advert_type'] = advert_type
                fillingFields_Rental_House(soup, Declaration)
            elif (keyWords[2] == 'коммерческая недвижимость'):
                advert_type += 'офис'
                Declaration['advert_type'] = advert_type
                fillingFields_Rental_Commercial(soup, Declaration)
            elif (keyWords[2] == 'нежилая недвижимость'):
                if (keyWords[3] == 'гаражи, стоянки'):
                    advert_type += 'гараж, стоянку'
                    Declaration['advert_type'] = advert_type
                    fillingFields_Rental_nonResidential_Garage(soup, Declaration)
                elif (keyWords[3] == 'дачи, участки '):
                    advert_type += 'дачу, участок'
                    Declaration['advert_type'] = advert_type
                    fillingFields_Rental_nonResidential_Dacha(soup, Declaration)
    else:
        pass

    return Declaration


def info():
    return PARSER_NAME


def parse(queue):
    html = get_html(baseUrl + '/nedvizimost/?page=1')
    countPage = getPageCount(html)

    for i in range(1, countPage + 1):
        urlPage = baseUrl + '/nedvizimost/?page=' + str(i)
        html = get_html(urlPage)
        soup = BeautifulSoup(html, 'html.parser')
        divContent = soup.find('div', class_='col-md-9 cusDirStyle')
        massItm = divContent.findAll('div', class_='col-md-12 catItem unpad premium')
        massItm = massItm + divContent.findAll('div', class_='col-md-12 catItem unpad')

        for j in massItm:
            a = j.find('a')
            href = a['href']
            urlDeclaration = baseUrl + href
            subHeading = j.find('div', class_='col-md-9 unpad')
            htmlDeclaration = get_html(urlDeclaration)
            result_data = parsing(htmlDeclaration, urlDeclaration, subHeading)
            queue.put_nowait((PARSER_NAME, result_data))
    queue.put_nowait(('stop:' + PARSER_NAME, None))
