from configparser import ConfigParser
from time import gmtime, strftime, sleep
from re import compile, search
from bs4 import BeautifulSoup
from random import choice, uniform
import requests
import json

PARSER_NAME = 'bepspb'


def get_curr_time():
    return strftime("%Y-%m-%d-%H-%M-%S", gmtime())


class GetAllBase(object):
    def __init__(self, queue):
        self.queue = queue
        self.url = ''
        self._settings()
        self.need_more = False

    def _settings(self):
        config = ConfigParser()
        config.read('./extra/config.conf')
        self.headers = {
            'Host': 'bepspb.ru',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
            'Referer': 'http://bepspb.ru/',
            'X-Requested-With': 'XMLHttpRequest',
            'X-MicrosoftAjax': 'Delta=true'
        }

        self.data = {
            '__EVENTTARGET': config.get('Main', '__EVENTTARGET'),
            '__EVENTVALIDATION': config.get('Main', '__EVENTVALIDATION'),
            '__CVIEWSTATE': config.get('Main', '__CVIEWSTATE'),
        }
        self._id = compile('/(\d+)/$')
        self.address = compile('(?:адрес[у]?:|Адрес [(]местоположение[)]:) ([^;(]+)')
        self.area = compile('([\d,. ]+)кв[.]?')
        self.cadastre = compile('\d{2}:\d{2}:\d{1,7}:\d+')

    def _get_pages(self, start_page):
        for page in range(start_page, 13):
            self.data['__EVENTTARGET'] = self.data['__EVENTTARGET'][:-2] + f'{page}'.zfill(2)
            self.get_items()

    def parse(self):
        self._get_pages(1)

    def get_items(self):
        counter = 0

        sleep(uniform(3, 6))
        resp = requests.post('http://www.bepspb.ru/', headers=self.headers, data=self.data)
        soup = BeautifulSoup(resp.text, 'lxml')

        for item in soup.find_all('tr', class_="gridRow"):
            _, lot, price, end_date, status, desc = [i.text.strip() for i in item.find_all('td', class_="gridAltColumn")]
            top = item.find('a', class_="tip-lot")
            title = top.text.strip().replace('\n', '').replace('\r', ';')
            link = top.get('href')
            _id = self._id.search(link).group(1)
            address = self.address.search(title)
            area = self.area.search(title)
            cadastre = self.cadastre.search(title)
            if not cadastre:
                cadastre = search('\d{2}-\d{2}-\d{2}/\d{3}/\d{4}-\d+', title)
            if area or address or cadastre:
                address = address.group(1) if address else address
                area = area.group(1).replace(' ', '').replace('.', ',') if area else area
                cadastre = cadastre.group(0) if cadastre else cadastre

                lotData = {
                    'FOREIGN_ID': _id,
                    'LOT_NUMBER': lot,
                    'LOT_LINK': link,
                    'PRICE': price.replace(' ', ''),
                    'LOT_END_DATE': end_date,
                    'LOT_STATUS': status,
                    'TITLE': title,
                    'ADDRESS': address,
                    'TOTAL_AREA': area,
                }

                if (cadastre):
                    lotData['CADASTRE'] = cadastre

                self.queue.put_nowait((PARSER_NAME, lotData))

                counter += 1
        self.data['__CVIEWSTATE'] = search('.*name=\"__CVIEWSTATE\".*?value=\"(.+?)\".*?', resp.text).group(1)
        self.data['__EVENTVALIDATION'] = search('.*name=\"__EVENTVALIDATION\".*?value=\"(.+?)\".*?', resp.text).group(1)

def info():
    return PARSER_NAME

def parse(obj):
    a = GetAllBase(obj)
    a.parse()
    obj.put_nowait(('stop:' + PARSER_NAME, None))
