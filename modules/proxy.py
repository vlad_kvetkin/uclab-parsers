import requests
from lxml import html


class Proxy:
    proxyUrl = 'http://www.ip-adress.com/proxy_list'
    proxyIpList = []
    proxyPortList = []
    usedProxyList = []

    def __init__(self):
        request = requests.get(self.proxyUrl)
        content = html.fromstring(request.content)
        ports = content.xpath("//tr/td[1]/text()")
        proxyIps = content.xpath("//tr/td[1]/a/text()")
        self.proxyPortList = ports
        self.proxyIpList = proxyIps

    def get_proxy(self):
        i = 0
        for proxyIp in self.proxyIpList:
            url = 'http//' + proxyIp + self.proxyPortList[i]
            if url in self.usedProxyList:
                continue
            try:
                r = requests.get('https://ya.ru/', proxies={'http': url})
                if r.status_code == 200:
                    self.usedProxyList.append(url)
                    return {"http": url}
            except requests.exceptions.ConnectionError:
                i += 1
                continue
