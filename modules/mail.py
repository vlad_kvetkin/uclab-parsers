from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import json

PARSER_NAME = 'restate'


def info():
    return PARSER_NAME


def parser(url, type):
    list = {}
    list['advert_type'] = type

    html = requests.get(url)
    soup = BeautifulSoup(html.text, 'html.parser')

    zagolovok = soup.find('div', class_='cols__wrapper')

    zagolovoch = zagolovok.find('h1', class_='hdr__inner').text
    list['Заголовок'] = zagolovoch

    adress = zagolovok.find('span', class_='hdr__inner').text
    list['address'] = adress

    prodav = soup.find('span', class_='p-contact__name').text.replace('\"', '')
    list['seller'] = prodav

    # Симуляция клика для показа полного номера
    options = webdriver.FirefoxOptions()
    options.add_argument('-headless')
    driver = webdriver.Firefox(firefox_options=options)
    driver.get(url)

    link = driver.find_element_by_link_text("Показать")
    link.click()

    phone = driver.find_element_by_class_name('p-contact__phone').text
    driver.close()
    phone = soup.find('span', class_='p-contact__phone').text
    list['seller_phone'] = phone

    data = soup.find('span', class_='note__text js-ago').text
    list['date_of_public'] = data

    tablel = soup.find('div', class_='cols__column cols__column_small_12 cols__column_medium_12 cols__column_large_12')
    table = tablel.find('div', class_='cols__inner')

    price = table.find('span', class_='hdr__inner').text.replace('\xa0', ' ')
    list['price'] = price

    try:
        price_for_m = table.find('span', class_='note__text').text.replace('\xa0', ' ')
        list['price_per_m2'] = price_for_m
    except:
        list['price_per_m2'] = "Не указано"

    try:
        district = table.find('span', class_='link__text').text.strip()
        list['address'] += ', ' + district
    except:
        list['address'] += ', Район не указан'

    try:
        etaz = soup.findAll('span', {'class': 'p-params__text'})[0].text.replace('/', ' из ')
        list['floor'] = etaz
    except:
        list['floor'] = "Не указано"

    try:
        colvo_room = soup.findAll('span', {'class': 'p-params__text'})[1].text
        list['room_count'] = colvo_room
    except:
        list['room_count'] = "Не указано"

    try:
        ploshad_o = soup.findAll('span', {'class': 'p-params__text'})[2].text
        list['total_area'] = ploshad_o
    except:
        list['total_area'] = "Не указано"

    try:
        ploshad_z = soup.findAll('span', {'class': 'p-params__text'})[3].text
        list['living_area'] = ploshad_z
    except:
        list['living_area'] = "Не указано"

    try:
        ploshad_k = soup.findAll('span', {'class': 'p-params__text'})[4].text
        list['kitchen_area'] = ploshad_k
    except:
        list['kitchen_area'] = "Не указано"

    promezutok = soup.find('div', {
        'class':
        'cols__column cols__column_small_19 cols__column_medium_30 cols__column_large_34 js-track_visibility'
    })
    try:
        opisanie = promezutok.find('div', {'class': ''}).text
        list['Описание'] = opisanie
    except:
        list['Описание'] = "Не указано"

    vyrez = soup.findAll('div', {'class': 'cols__wrapper'})[4]

    # Для стандартизации придется подробнее анализировать страницу
    # В этом блоке парсится доп информация (тип дома, балкон, лифт и т.д.)
    name = vyrez.findAll("span", {"class": "p-params__name color_gray"})
    value = vyrez.findAll("span", {"class": "p-params__text"})

    for i in name:
        for e in value:
            featureName = i
            featureValue = e
            inf = ''.join(featureName.get_text().encode(html.encoding).decode('utf-8'))
            if (inf == 'Тип дома'):
                list['home_type'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Тип продажи'):
                list['sale_type'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Балконов' or inf == 'Лоджий'):
                list['balcony'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Лифт' or inf == 'Пассажирских лифтов' or inf == 'Грузовых лифтов'):
                list['elevator'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Тип санузла'):
                list['bathroom_type'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Раздельных санузлов' or inf == 'Совмещенных санузлов'):
                list['bathroom_count'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Вид из окон'):
                list['window_view'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Во двор'):
                list['window_view'] = 'Во двор'
            elif (inf == 'На улицу'):
                list['window_view'] = 'На улицу'
            elif (inf == 'Год постройки'):
                list['year_of_constr'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Высота потолков'):
                list['ceiling_height'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Площадь комнат'):
                list['room_area'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Ремонт'):
                list['repairs'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))
            elif (inf == 'Мебель'):
                list['furniture'] = ''.join(featureValue.get_text().encode(html.encoding).decode('utf-8'))

            else:
                list[''.join(featureName.get_text().encode(html.encoding).decode('utf-8'))] = ''.join(
                    featureValue.get_text().encode(html.encoding).decode('utf-8'))
            del value[0]
            break

    return list


def getPageCount(html):
    rrr = requests.get(html)
    soup = BeautifulSoup(rrr.text, 'html.parser')
    paggination = soup.find('div', class_='paging js-paging')
    return int(paggination.find_all('a')[-1].text)


def scanPage(url, queue):
    html1 = requests.get(url)
    soup1 = BeautifulSoup(html1.text, 'html.parser')
    offerURLs = soup1.findAll("a", {"class": "p-instance__title link-holder"})
    i = 0
    while i != 15:
        for item in offerURLs:
            href = item.attrs["href"]
            type = "Продажа"
            data = parser(href, type)
            queue.put_nowait((PARSER_NAME, data))
            i += 1


def parse(queue):
    url = "https://vlgg.realty.mail.ru/sale/living/"
    pageCount = getPageCount(url)
    for page in range(1, pageCount):
        https = (url + '?page=%d' % page)
        scanPage(https, queue)
    queue.put_nowait(('stop:' + PARSER_NAME, None))
