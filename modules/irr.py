from bs4 import BeautifulSoup
import requests
import time
import re

PARSER_NAME = 'irr'


def info():
    return PARSER_NAME


def parse(queue):
    numberOfRecords = 0
    irrPageOne = "http://russia.irr.ru/real-estate/"
    request = requests.get(irrPageOne)
    soup = BeautifulSoup(request.text, 'html.parser')
    # страница со всеми разделами
    advertisementType = ""
    homeType = "Жилая недвижимость"
    homeTypeEnumId = 0
    for mainPageBlock in soup.find_all('div', 'mainPageBlockCategory__wrapper'):
        if homeTypeEnumId == 3 or homeTypeEnumId == 4:
            homeType = "Коммерческая недвижимость"
        if homeTypeEnumId == 5 or homeTypeEnumId == 6:
            homeType = "Загородная недвижимость"
        if homeTypeEnumId == 7 or homeTypeEnumId == 8:
            homeType = "Гаражи и стоянки"
        homeTypeEnumId += 1
        advertisementType = mainPageBlock.find_all("a")[0].text
        pageUrl = "http://irr.ru" + mainPageBlock.find_all("a")[0].attrs["href"]
        request = requests.get(pageUrl)
        soup = BeautifulSoup(request.text, 'html.parser')
        countStr = soup.find_all('li', 'pagination__pagesItem')
        koli4_str = countStr[len(countStr) - 1]
        last_url = koli4_str.find("a").attrs["href"]
        last_url_1 = last_url[:-(len(last_url) - last_url.find("page") - 4)]
        i1 = int(koli4_str.text)
        i = 0
        # по страницам снизу
        while i < 5:
            i += 1
            odin_url_nomer_str = "http://irr.ru" + last_url_1 + str(i) + "/"
            r_123 = requests.get(odin_url_nomer_str)
            soup_123 = BeautifulSoup(r_123.text, 'html.parser')
            # по списку с КВ
            for item2_1 in soup_123.find_all('div', 'listing__itemTitleWrapper'):
                for a_elm in item2_1.find_all("a"):
                    url_page = a_elm.attrs["href"]
                    r = requests.get(url_page)
                    soup = BeautifulSoup(r.text, 'html.parser')
                    # СТРАНИЦА
                    data_error = {}
                    data = {}
                    koli4estvo_error = 0
                    data["HOME_TYPE"] = homeType
                    data["ADVERTISEMENT_TYPE"] = advertisementType
                    data["ADVERTISEMENT_URL"] = url_page
                    try:
                        list_img = []
                        for image in soup.find('div', class_='lineGallery js-lineProductGallery').find_all('meta'):
                            list_img.append(image.attrs['content'])
                        data["ADVERTISEMENT_PHOTO_URL"] = list_img
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "картинки"
                    try:
                        address = soup.find('div','productPage__infoTextBold js-scrollToMap').text
                        address = str(address)
                        address = re.sub('\s+', ' ', address)
                        data['ADDRESS'] = address
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "адресс"
                    try:
                        name = soup.find('div', "productPage__infoTextBold productPage__infoTextBold_inline").text
                        name = str(name)
                        name = re.sub('\s+', ' ', name)
                        data['SELLER_INFO'] = name
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "Контактное лицо"
                    try:
                        header = soup.find('h1', 'productPage__title').text
                        header = re.sub('\s+', ' ', header)
                        data["TITLE"] = header
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "хэдер"
                    try:
                        price = soup.find('div', 'productPage__price').text
                        price = re.sub('\s+', ' ', price)
                        data["PRICE"] = price
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "цена"
                    try:
                        data['SELLER_PHONE'] = re.sub('\s+', ' ', soup.find('div', 'productPage__phoneText').text)
                        tt1 = soup.find('div', 'productPage__inlineWrapper')
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "номер"

                    try:
                        opisanie_k = soup.find('p', 'productPage__descriptionText').text
                        opisanie_k = re.sub('\s+', ' ', opisanie_k)
                        data["DESCRIPTION"] = opisanie_k
                    except:
                        koli4estvo_error += 1
                        data_error[str(numberOfRecords) + ":" + str(koli4estvo_error)] = "описание"
                    try:
                        numberOfRecords += 1
                        if koli4estvo_error > 0:
                            data_error["url"] = url_page
                        queue.put_nowait((PARSER_NAME, data))
                    except:
                        data_error[str(numberOfRecords) + ":" + "save"] = "save"
                        time.sleep(10)

    queue.put_nowait(('stop:' + PARSER_NAME, None))
