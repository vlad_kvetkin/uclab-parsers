from bs4 import BeautifulSoup
import requests
import time
import json
import re

adverts_mas = []

PARSER_NAME = 'kvadroom'

# Адреса страниц исходя из сделанной структуры(жилые)
ADVERTS_TYPE_0 = {
    'Купить квартиру': 'kupit-kvartiru/?page=\t',
    'Снять квартиру': 'sniat-kvartiru/?page=\t',
    'Снять посуточно квартиру': 'kvartiri-posutochno/?page=\t',
    'Купить комнату': 'kupit-komnatu/?page=\t',
    'Снять комнату': 'sniat-komnatu/?page=\t',
    'Купить таунхаус': 'kupit-taunhaus/?page=\t',
    'Снять таунхаус': 'sniat-taunhaus/?page=\t',
    'Купить дом': 'kupit-dom/?page=\t',
    'Снять дом': 'sniat-dom/?page=\t',
    'Снять посуточно дом': 'sniat-kottedji-posutochno/?page=\t',
    'Купить участок': 'zemelnie-uchastki/?page=\t'
}
# Адреса страниц исходя из сделанной структуры(нежилые)
ADVERTS_TYPE_1 = {
    'Купить гараж': 'prodaja-garajey-i-mashinomest/?page=\t',
    'Снять гараж': 'garaji-i-mashinomesta-v-arendu/?page=\t',
    'Купить офис': 'prodaja-ofisov/?page=\t',
    'Снять офис': 'arenda-ofisov/?page=\t',
    'Купить торговую площадь': 'prodaja-torgovih-ploshadey/?page=\t',
    'Снять торговую площадь': 'arenda-torgovih-ploshadey/?page=\t',
    'Купить склад': 'prodaja-slkadov/?page=\t',
    'Снять склад': 'arenda-skladov/?page=\t',
    'Купить помещение общественного питания': 'prodaja-pomesheniy-obshestvennogo-pitaniya/?page=\t',
    'Снять помещение общественного питания': 'arenda-pomesheniy-obshestvennogo-pitaniya/?page=\t',
    'Купить готового бизнеса': 'prodaja-gotovogo-biznesa/?page=\t',
    'Аренда готового бизнеса': 'gotoviy-biznes-v-arendu/?page=\t',
    'Купить помещение свободного назначения': 'prodaja-psn/?page=\t',
    'Снять помещение свободного назначения': 'arenda-psn/?page=\t',
}


# Основная(верхняя) информация с объявлений(жилые)
def get_residential(url, ad_type):
    advert = {}
    advert['advert_type'] = ad_type  # Тип задается при вызове исходя из заданного раздела

    html = requests.get(url)
    html.encoding = 'utf-8'
    soup = BeautifulSoup(html.text, 'html.parser')

    # Дата подачи объявления
    date = soup.find_all('ul', 'ob2_obj_short_info')[0].find_all('li')[1].text
    date = re.findall('\\d+.\\d+.\\d+', date)[0]
    if (len(date) > 1):
        advert['date_of_public'] = date.strip()

    information = soup.find_all('ul', 'u_ob2_dot_list')

    # Кол-во комнат в квартире(при учете выбора квартиры)
    if (ad_type.split().count('квартиру') == 1):
        rooms = soup.find_all('ul', 'u_ob2_dot_list')[0].find_all('li')[0].text.strip()
        if (len(rooms) > 0):
            rooms = rooms[0].split('-')[0]

    # Адрес
    address = soup.find_all('ul', 'ob2_obj_adress_v2')
    if (len(address) > 0):
        address = address[0].find_all('a')
        addr = ', '.join([address[elem].text for elem in range(len(address))])
        if (ad_type.split().count('квартиру') == 1 or type.split().count('комнату') == 1):
            street_house = soup.find_all('h1', 'u_fs_h1_adaptive u_med')[0].text.split(',')
            addr = addr + ',' + street_house[2].strip() + ', ' + street_house[3].strip()
            advert['address'] = addr
        else:
            street_house = soup.find_all('h1', 'u_fs_h1_adaptive u_med')[0].text.split(',')
            addr = addr + ',' + street_house[2].strip()

    # Цена
    price = soup.find('div', 'ob2_obj_price_v2__number u_fs_h1_adaptive')
    advert['price'] = price.text.replace(' ', '')

    # Парсинг основных свойств объекта
    if (ad_type.split().count('квартиру') == 1):
        # Площадь
        total_area = information[0].find_all('li')[1].split(' ')[0]
        living_area = information[0].find_all('li')[2].split(' ')[0]
        kitchen_area = information[0].find_all('li')[3].split(' ')[0]
        advert['total_area'] = total_area
        advert['living_area'] = living_area
        advert['kitchen_area'] = kitchen_area
        # Этаж
        floor = information[0].find_all('li')[4].split(' ')[0]
        total_floor = information[0].find_all('li')[4].split(' ')[3]
        advert['floor'] = floor
        advert['total_floor'] = total_floor
        # Санузел
        bathroom_type = information[0].find_all('li')[5].split(' ')[0]
        advert['bathroom_type'] = bathroom_type
    elif (ad_type.split().count('комнату') == 1):
        room_area = information[0].find_all('li')[1].split(' ')[0]
        kitchen_area = information[0].find_all('li')[2].split(' ')[0]
        advert['room_area'] = room_area
        advert['kitchen_area'] = kitchen_area
        # Этаж
        floor = information[0].find_all('li')[3].split(' ')[0]
        total_floor = information[0].find_all('li')[3].split(' ')[3]
        advert['floor'] = floor
        advert['total_floor'] = total_floor
        # Санузел
        bathroom_type = information[0].find_all('li')[4].split(' ')[0]
        advert['bathroom_type'] = bathroom_type
    elif (ad_type.split().count('дом') == 1):
        # Площадь
        total_area = information[0].find_all('li')[0].split(' ')[0]
        advert['total_area'] = total_area
        total_floor = information[0].find_all('li')[1].split(' ')[0]
        advert['total_floor'] = total_floor

    # Текстовое описание
    text = str(soup.find_all('div', '!js_slice_text')[0])
    text = text.split('<div class="!js_slice_text" itemprop="description">')[1]
    advert['description'] = (' '.join(text.split('<br/>'))).strip()

    # Контакты(номер телефона)
    phone = soup.find_all('u_ob2_btn js_show_all_phone')[0].get('data-link-tel').replace('tel:', '')
    if (len(phone) > 0):
        advert['seller_phone'] = phone.strip()

    return advert


#Основная(верхняя) информация с объявлений(нежилые)
def get_commercial(url, ad_type):
    advert = {}
    advert['advert_type'] = ad_type  # Тип задается при вызове исходя из заданного раздела

    html = requests.get(url)
    html.encoding = 'utf-8'
    soup = BeautifulSoup(html.text, 'html.parser')

    # Дата подачи объявления
    date = soup.find_all('ul', 'ob2_obj_short_info')[0].find_all('li')[1].text
    date = re.findall('\\d+.\\d+.\\d+', date)[0]
    if (len(date) > 1):
        advert['date_of_public'] = date.strip()

    # Площадь
    heading = soup.find_all('h1', 'u_fs_h1_adaptive u_med')[0]
    advert['total_area'] = re.findall('\\d+', heading.text)[0]

    # Адрес
    address = soup.find_all('ul', 'ob2_obj_adress_v2')
    if (len(address) > 0):
        address = address[0].find_all('a')
        addr = ', '.join([address[elem].text for elem in range(len(address))])
        street_house = heading.text.split(',')
        if (len(street_house) == 3):
            addr = addr + ', ' + street_house[2].strip()
        elif (len(street_house) == 4):
            addr = addr + ', ' + street_house[2].strip() + ', ' + street_house[3].strip()
        advert['address'] = addr
    # Цена
    price = soup.find('div', 'ob2_obj_price_v2__number u_fs_h1_adaptive')
    advert['price'] = price.text.replace(' ', '')

    # Парсинг основных свойств объекта
    properties = soup.find_all('ul', 'u_ob2_dot_list')
    if (len(properties) > 0):
        properties = properties[0].find_all('li')
        advert[properties] = ' '.join(properties.find('td').text.split())

    # Текстовое описание
    text = str(soup.find_all('div', '!js_slice_text')[0])
    text = text.split('<div class="!js_slice_text" itemprop="description">')[1]
    advert['description'] = (' '.join(text.split('<br/>'))).strip()

    # Контакты(номер телефона)
    phone = soup.find_all('u_ob2_btn js_show_all_phone')[0].get('data-link-tel').replace('tel:', '')
    if (len(phone) > 0):
        advert['seller_phone'] = phone.strip()

    return advert


def info():
    return PARSER_NAME


def parse(queue):
    # Главная страница
    # Актуально для Волгоградской области. Если нужен другой регион - заменить значение
    base_url = 'https://volg.kvadroom.ru/'
    # Жилая недвижимость
    prop_type = 0
    headers = {
        'User-Agent':
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
    }

    for ad_type in ADVERTS_TYPE_0.keys():
        page_number = 1
        last_page = 2

        while last_page >= page_number:
            page_url = base_url + str(page_number).join(ADVERTS_TYPE_0.get(ad_type).split('\t'))
            page = requests.get(page_url, headers=headers)
            soup = BeautifulSoup(page.text, 'html.parser')
            ad_list = soup.find_all("a", class_="card_item")
            for ad in ad_list:
                result_data = get_residential(ad['href'], ad_type)
                queue.put_nowait((PARSER_NAME, result_data))

            # возвращаем номер последней доступной отсюда страницы или -1, если страниц еще очень много
            pages = soup.find_all('div', 'pagination_block')
            if (len(pages) > 0):
                pages = pages[0].contents
                last_page = pages[len(pages) - 2].text
                try:
                    last_page = int(last_page)
                except:
                    last_page = -1
            else:
                last_page = 1  # Если нет списка страниц, то страница одна
            page_number += 1
            time.sleep(5)

    # Не жилая недвижимость
    prop_type = 1
    for key in ADVERTS_TYPE_1.keys():
        page_number = 1
        last_page = 2
        while last_page >= page_number:
            page_url = base_url + str(page_number).join(ADVERTS_TYPE_1.get(key).split('\t'))
            page = requests.get(page_url, headers=headers)
            soup = BeautifulSoup(page.text, 'html.parser')
            ad_list = soup.find_all('a', 'card_item')
            for ad in ad_list:
                queue.put_nowait((PARSER_NAME, json.dumps(get_commercial(ad['href'], ad_type))))
            # возвращаем номер последней доступной отсюда страницы или -1, если страниц еще очень много
            pages = soup.find_all('div', 'pagination_block')
            if (len(pages) > 0):
                pages = pages[0].contents
                last_page = pages[len(pages) - 2].text
                try:
                    last_page = int(last_page)
                except:
                    last_page = -1
            else:
                last_page = 1  # Если нет списка страниц, то страница одна
            time.sleep(5)
            page_number += 1
    queue.put_nowait(('stop:' + PARSER_NAME, None))
