from datetime import datetime
from bs4 import BeautifulSoup
import requests
import json
import re
import time

PARSER_NAME = 'roseltorg'


def get_url(tp, count):
    if (tp == 'f'):
        url = 'https://sale.roseltorg.ru/trade/future/?page=1&limit=' + str(count)
    if (tp == 'p'):
        url = 'https://sale.roseltorg.ru/trade/past/?page=1&limit=' + str(count)
    if (tp == 'c'):
        url = 'https://sale.roseltorg.ru/trade/failed/?page=1&limit=' + str(count)
    return url


def get_adv_count(type_):
    if (type_ == 'f'):
        html = requests.get('https://sale.roseltorg.ru/trade/future').text
        soup = BeautifulSoup(html, 'html.parser')
        count = soup.find(
            'ul', class_=' right_lenk_page link_page_bot_lst right_lelt_page_all').find_all('strong')[2].text

    if (type_ == 'p'):
        html = requests.get('https://sale.roseltorg.ru/trade/past').text
        soup = BeautifulSoup(html, 'html.parser')
        count = soup.find(
            'ul', class_=' right_lenk_page link_page_bot_lst right_lelt_page_all').find_all('strong')[2].text

    if (type_ == 'c'):
        html = requests.get('https://sale.roseltorg.ru/trade//failed').text
        soup = BeautifulSoup(html, 'html.parser')
        count = soup.find(
            'ul', class_=' right_lenk_page link_page_bot_lst right_lelt_page_all').find_all('strong')[2].text

    return count


def get_adv_list(url):
    id_list = []
    url_list = []
    html = requests.get(url).text

    id_list = re.findall(r'\?id=\w{1,5}\d+', html)
    url_string = 'https://sale.roseltorg.ru/trade/view/'
    for str in id_list:
        url_list.append(url_string + str)
    return url_list


def info():
    return PARSER_NAME


def _parse(url):
    html = requests.get(url).text
    soup = BeautifulSoup(html, 'html.parser')
    list = {}
    soup = soup.find(class_='text_right')
    data_blocks = soup.find_all(class_='data-block')

    # цикл парсит первые два информационных блока
    for index in range(0, 2):
        search = data_blocks[index].find_all('p')
        i = 0
        for p in search:
            if ((i % 2 == 0)):
                list[search[i].text] = search[i + 1].text
            i = i + 1

    # Здесь парсятся ссылки на документы
    # Начинаем со второго элемента так как 0 и 1 - инф блоки
    for index in range(2, len(data_blocks)):
        # Находим все элементы с ссылками
        search = data_blocks[index].find_all('a')

        for index in range(0, len(search)):
            list[search[index].text] = search[index].get('href')
    return (list)


def parse(pipe):
    count_f = get_adv_count('f')
    count_p = get_adv_count('p')
    count_c = get_adv_count('c')

    while True:
        main_list = {}

        if (count_f < get_adv_count('f')):
            url = get_url('f', int(get_adv_count('f')) - int(count_f))
            url_list = get_adv_list(url)
            index = 0

            for str in url_list:
                main_list[index] = _parse(str)
                index = index + 1

            pipe.put_nowait((PARSER_NAME, main_list))

        if (int(count_p) < int(get_adv_count('p'))):

            url = get_url('p', int(get_adv_count('p')) - int(count_p))
            url_list = get_adv_list(url)
            index = 0

            for string in url_list:
                main_list[index] = _parse(string)
                index = index + 1

            pipe.put_nowait((PARSER_NAME, main_list))

        if (count_c < get_adv_count('c')):
            url = get_url('c', int(get_adv_count('c')) - int(count_c))
            url_list = get_adv_list(url)
            index = 0

            for str in url_list:
                main_list[index] = _parse(str)
                index = index + 1

            pipe.put_nowait((PARSER_NAME, main_list))

        count_f = get_adv_count('f')
        count_p = get_adv_count('p')
        count_c = get_adv_count('c')
        time.sleep(300)
    pipe.put_nowait(('stop:' + PARSER_NAME, None))
